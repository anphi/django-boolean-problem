from django.forms import ModelForm, RadioSelect
from .models import TestModel


class BoolForm(ModelForm):
    class Meta:
        model = TestModel
        fields = ['boolfield']
        widgets = {'boolfield': RadioSelect()}