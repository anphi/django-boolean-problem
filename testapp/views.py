from django.shortcuts import render
from .forms import BoolForm


# Create your views here.
def index(request):
    f = BoolForm()
    return render(request, 'testapp/index.html', {'f': f})
