from django.db import models

BOOLEAN_YN = (
    (None, ''),
    (True, u'YES'),
    (False, u'NO'),
)


# Create your models here.

class TestModel(models.Model):
    boolfield = models.BooleanField(null=False, blank=False, choices=BOOLEAN_YN)
